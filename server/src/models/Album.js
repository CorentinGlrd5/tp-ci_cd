const { DataTypes } = require('sequelize');
const sequelize = require('../db');

const Album = sequelize.define(
  'albums',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    date: {
      type: DataTypes.STRING,
    },
    image: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

module.exports = Album;

const Title = require('./Title');
const Artist = require('./Artist');
const User = require('./User');

Album.hasMany(Title);
Album.belongsTo(Artist);
Album.belongsToMany(User, { through: 'users_albums', timestamps: false });
