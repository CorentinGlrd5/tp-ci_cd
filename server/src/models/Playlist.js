const { DataTypes } = require('sequelize');
const sequelize = require('../db');

const Playlist = sequelize.define(
  'playlists',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

module.exports = Playlist;

const Title = require('./Title');
const User = require('./User');

Playlist.belongsToMany(Title, { through: 'playlists_titles', timestamps: false });
Playlist.belongsToMany(User, { through: 'users_playlists', timestamps: false });
