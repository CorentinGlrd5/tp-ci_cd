const { DataTypes } = require('sequelize');
const sequelize = require('../db');

const Artist = sequelize.define(
  'artists',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    image: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

module.exports = Artist;

const Title = require('./Title');
const Album = require('./Album');
const User = require('./User');

Artist.hasMany(Title);
Artist.hasMany(Album);
Artist.belongsToMany(User, { through: 'users_artists', timestamps: false });
