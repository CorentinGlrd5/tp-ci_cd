const { DataTypes } = require('sequelize');
const sequelize = require('../db');

let User = sequelize.define(
  'users',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    username: {
      type: DataTypes.STRING,
    },
    password: {
      type: DataTypes.STRING,
    },
    avatar: {
      type: DataTypes.STRING,
    },
    role: {
      type: DataTypes.STRING,
    },
  },
  { timestamps: false }
);

module.exports = User;

const Title = require('./Title');
const Playlist = require('./Playlist');
const Artist = require('./Artist');
const Album = require('./Album');

User.belongsToMany(Title, { through: 'users_titles', timestamps: false });
User.belongsToMany(Album, { through: 'users_albums', timestamps: false });
User.belongsToMany(Artist, { through: 'users_artists', timestamps: false });
User.belongsToMany(Playlist, { through: 'users_playlists', timestamps: false });
