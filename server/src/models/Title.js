const { DataTypes } = require('sequelize');
const sequelize = require('../db');

const Title = sequelize.define(
  'titles',
  {
    id: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
    },
    time: {
      type: DataTypes.INTEGER,
    },
  },
  { timestamps: false }
);

module.exports = Title;

const Artist = require('./Artist');
const Album = require('./Album');
const Playlist = require('./Playlist');
const User = require('./User');

Title.belongsTo(Artist);
Title.belongsTo(Album);
Title.belongsToMany(Playlist, { through: 'playlists_titles', timestamps: false });
Title.belongsToMany(User, { through: 'users_titles', timestamps: false });
