const request = require("supertest");
const express = require("express");
const app = express();
const User = require("../models/User");

describe("API testing", () => {
  const userBody = {
    username: "corentin",
    password: "Coco12345",
  };

  it("Post signup", (done) => {
    const expectedResponse = {};
    request(app).post("/signup").send(userBody).expect(200).end((err, res) => {
      expect(res.body).toEqual(expectedResponse);
      done();
    });
  });

  it("Post signin", (done) => {
    const expectedResponse = {};
    request(app).post("/signin").send(userBody).expect(200).end((err, res) => {
      expect(res.body).toEqual(expectedResponse);
      done();
    });
  });
});
