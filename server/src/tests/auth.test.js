const request = require("supertest");
const testConnexion = require("../testConnexion");

describe("API testing", () => {
  let creatUser = {
    username: "Corentin",
    password: "Coco12345",
  };
  let signinUserfalsePassword = {
    username: "Corentin",
    password: "Coco5283288",
  };
  let ResponseUserError400 = { error: "This account already exist" };
  let ResponseUserSigninError404 = { error: "This account doesn't exist" };
  let ResponseUserSigninError403 = { error: "Password is not valid" };

  // If users not exist in database
  it("Post users signin - 404", async (done) => {
    const response = await request(testConnexion).post("/signin").send(creatUser);
    expect(response.status).toBe(404);
    expect(response.body).toStrictEqual(ResponseUserSigninError404);
    done();
  });

  // If users not exist in database
  it("Post users signup - status: 200", async (done) => {
    const response = await request(testConnexion).post("/signup").send(creatUser);
    expect(response.status).toBe(200);
    done();
  });

  // If user exist in database
  it("Post users signup - error 400", async (done) => {
    const response = await request(testConnexion).post("/signup").send(creatUser);
    expect(response.status).toBe(400);
    expect(response.body).toStrictEqual(ResponseUserError400);
    done();
  });

  // If user exist in database
  it("Post users signin - status: 200", async (done) => {
    const response = await request(testConnexion).post("/signin").send(creatUser);
    expect(response.status).toBe(200);
    done();
  });

  // If users exist in database but password is false
  it("Post users signin - 403", async (done) => {
    const response = await request(testConnexion).post("/signin").send(signinUserfalsePassword);
    expect(response.status).toBe(403);
    expect(response.body).toStrictEqual(ResponseUserSigninError403);
    done();
  });
});
