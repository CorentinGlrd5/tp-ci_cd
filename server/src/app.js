const express = require('express');
const db = require('./db');
const seed = require('./seed');
require('dotenv').config();

const app = express();
const cors = require('cors');

async function connect(db) {
  try {
    await db.authenticate();
    await db.sync({ force: true, cascade: true });
    seed();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

connect(db).catch((error) => console.log(error));

app.use(express.json());
app.use(cors());

app.use('/users', require('./routes/users'));
app.use('/', require('./routes/auth'));
app.use('/albums', require('./routes/albums'));
app.use('/artists', require('./routes/artists'));
app.use('/playlists', require('./routes/playlists'));
app.use('/titles', require('./routes/titles'));

app.listen(process.env.API_PORT, () =>
  console.log(`App listening on port ${process.env.API_PORT}!`)
);

module.exports = app;
