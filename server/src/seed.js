const Title = require('./models/Title');
const Artist = require('./models/Artist');
const Album = require('./models/Album');
const User = require('./models/User');

const seed = () => {
  const albums = require('./data/albums.json');
  const artists = require('./data/artists.json');
  const titles = require('./data/titles.json');
  const users = require('./data/users.json');

  insertByModel(artists, Artist);
  insertByModel(albums, Album);
  insertByModel(titles, Title);
  insertByModel(users, User);
};

function insertByModel(json, model) {
  try {
    json.forEach(async (item) => {
      await model.create(item);
    });
  } catch (error) {
    console.log(error);
  }
}

module.exports = seed;
