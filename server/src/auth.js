function verify(token, key, options) {
  return new Promise((resolve, reject) => {
    jwt.verify(token, key, options, (error, token) => {
      if (error) {
        reject(error);
      } else {
        resolve(token);
      }
    });
  });
}

// Get Authorization header, split the Bearer and the token and verify token validity
module.exports = async (req, res, next) => {
  let token = req.get('Authorization').split(" ")[1];
  let decoded;
  if (!token) {
    res.status(401)
  }
  try {
    decoded = await verify(token, "H7B6UBYEZw5hqnGIoPlJSWlSxao09BnN")
  } catch(err) {
    res.status(403)
  }
  next();
}

