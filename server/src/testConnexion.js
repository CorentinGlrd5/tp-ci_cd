const express = require('express');
const db = require('./db');
const seed = require('./seed');
require('dotenv').config();

const testConnexion = express();
const cors = require('cors');

async function connect(db) {
  try {
    await db.authenticate();
    console.log('Connection has been established successfully.');
  } catch (error) {
    console.error('Unable to connect to the database:', error);
  }
}

connect(db).catch((error) => console.log(error));

testConnexion.use(express.json());
testConnexion.use(cors());

testConnexion.use('/users', require('./routes/users'));
testConnexion.use('/', require('./routes/auth'));
testConnexion.use('/albums', require('./routes/albums'));
testConnexion.use('/artists', require('./routes/artists'));
testConnexion.use('/playlists', require('./routes/playlists'));
testConnexion.use('/titles', require('./routes/titles'));

module.exports = testConnexion;
