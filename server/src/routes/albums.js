const express = require('express');
const router = express.Router();
const Album = require('../models/Album');
const Artist = require('../models/Artist');
const Title = require('../models/Title');

router.get('/', async (req, res) => {
  try {
    let albums = await Album.findAll({ include: [Artist, Title] });
    res.status(200).json(albums);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let album = await Album.findOne({ where: { id }, include: [Artist, Title] });
    res.status(200).json(album);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/', async (req, res) => {
  try {
    const body = req.body;
    let album = await Album.create(body);
    res.status(200).json({ album });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let update = await Album.update({ where: { id } }, body);
    res.status(200).json({ update });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let destroy = await Album.destroy({ where: { id } });
    res.status(200).json({ destroy });
  } catch (error) {
    res.status(400).json({ error });
  }
});

module.exports = router;
