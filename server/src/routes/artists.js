const express = require('express');
const router = express.Router();
const Artist = require('../models/Artist');
const Album = require('../models/Album');
const Title = require('../models/Title');

router.get('/', async (req, res) => {
  try {
    let artists = await Artist.findAll({ include: [Album, Title] });
    res.status(200).json(artists);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let artist = await Artist.findOne({
      where: { id },
      include: [{ model: Album, include: [Title] }],
    });
    res.status(200).json(artist);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/', async (req, res) => {
  try {
    const body = req.body;
    let artist = await Artist.create(body);
    res.status(200).json({ artist });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let update = await Artist.update({ where: { id } }, body);
    res.status(200).json({ update });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let destroy = await Artist.destroy({ where: { id } });
    res.status(200).json({ destroy });
  } catch (error) {
    res.status(400).json({ error });
  }
});

module.exports = router;
