const express = require('express');
const router = express.Router();
const Playlist = require('../models/Playlist');
const Title = require('../models/Title');

router.get('/', async (req, res) => {
  try {
    let playlists = await Playlist.findAll({
      include: [Title],
    });
    res.status(200).json(playlists);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let playlist = await Playlist.findOne({
      where: { id },
      include: [Title],
    });
    res.status(200).json(playlist);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/:id/titles', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let playlist = await Playlist.findOne({ where: { id } });
    let title = await Title.findOne({ where: body });
    playlist.addTitles(title);
    res.status(200).json({ playlist });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let update = await Playlist.update({ where: { id } }, body);
    res.status(200).json({ update });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id/titles', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let playlist = await Playlist.findOne({ where: { id } });
    let title = await Title.findOne({ where: body });
    playlist.removeTitles(title);
    res.status(200).json(playlist);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let destroy = await Playlist.destroy({ where: { id } });
    res.status(200).json({ destroy });
  } catch (error) {
    res.status(400).json({ error });
  }
});

module.exports = router;
