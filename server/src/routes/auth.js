const express = require('express');
const router = express.Router();
const User = require('../models/User');
const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

router.post('/signin', async (req, res) => {
  const { username, password } = req.body;
  let user = await User.findOne({ where: { username } });
  if (user) {
    let match = await bcrypt.compare(password, user.password);
    if (match) {
      let token = jwt.sign({ id: user.id }, 'H7B6UBYEZw5hqnGIoPlJSWlSxao09BnN', {
        expiresIn: '15m',
      });
      res.status(200).json({ token, id: user.id });
    } else {
      res.status(403).json({ error: 'Password is not valid' });
    }
  } else {
    res.status(404).json({ error: "This account doesn't exist" });
  }
});

router.post('/signup', async (req, res) => {
  const { username, password } = req.body;
  try {
    let hash = await bcrypt.hash(password, 10);
    let user = await User.findOne({ where: { username } });
    if (user) {
      res.status(400).json({ error: 'This account already exist' });
    } else {
      let user = await User.create({ username, password: hash });
      res.status(200).json(user);
    }
  } catch (err) {
    console.log(err);
    res.status(500).json({ error: 'Service unavailable, please retry later' });
  }
});

module.exports = router;
