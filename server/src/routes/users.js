const express = require('express');
const router = express.Router();
const auth = require('../auth');
const User = require('../models/User');
const Playlist = require('../models/Playlist');
const Album = require('../models/Album');
const Artist = require('../models/Artist');
const Title = require('../models/Title');

router.get('/', async (req, res) => {
  try {
    let users = await User.findAll();
    res.status(200).json(users);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let user = await User.findOne({
      where: { id },
      include: [Playlist, { model: Album, include: [Artist] }, Artist, Title],
    });
    res.status(200).json(user);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/:id/playlists', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let playlist = await Playlist.create(body);
    user.addPlaylist(playlist);
    res.status(200).json(playlist);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/:id/albums', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let album = await Album.findOne({ where: body });
    user.addAlbum(album);
    res.status(200).json(album);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/:id/artists', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let artist = await Artist.findOne({ where: body });
    user.addArtist(artist);
    res.status(200).json(artist);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/:id/titles', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let title = await Title.findOne({ where: body });
    user.addTitle(title);
    res.status(200).json(title);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id/albums', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let album = await Album.findOne({ where: body });
    user.removeAlbums(album);
    res.status(200).json(album);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id/artists', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let artist = await Artist.findOne({ where: body });
    user.removeArtists(artist);
    res.status(200).json(artist);
  } catch (error) {
    console.log(error);
    res.status(400).json({ error });
  }
});

router.delete('/:id/titles', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let user = await User.findOne({ where: { id } });
    let title = await Title.findOne({ where: body });
    user.removeTitles(title);
    res.status(200).json(title);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let update = await User.update({ where: { id } }, body);
    res.status(200).json({ update });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let destroy = await User.destroy({ where: { id } });
    res.status(200).json({ destroy });
  } catch (error) {
    res.status(400).json({ error });
  }
});

module.exports = router;
