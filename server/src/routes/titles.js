const express = require('express');
const router = express.Router();
const Title = require('../models/Title');
const Artist = require('../models/Artist');
const Album = require('../models/Album');

router.get('/', async (req, res) => {
  try {
    let titles = await Title.findAll({ include: [Album, Artist] });
    res.status(200).json(titles);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.get('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let title = await Title.findOne({ where: { id }, include: [Album, Artist] });
    res.status(200).json(title);
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.post('/', async (req, res) => {
  try {
    const body = req.body;
    let title = await Title.create(body);
    res.status(200).json({ title });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.patch('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const body = req.body;
    let update = await Title.update({ where: { id } }, body);
    res.status(200).json({ update });
  } catch (error) {
    res.status(400).json({ error });
  }
});

router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    let destroy = await Title.destroy({ where: { id } });
    res.status(200).json({ destroy });
  } catch (error) {
    res.status(400).json({ error });
  }
});

module.exports = router;
