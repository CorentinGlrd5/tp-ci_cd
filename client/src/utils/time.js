/**
 * Convert a time in second to a more readable MM:SS time format
 *
 * @param {Number} time Time in seconds
 * @return Time in MM:SS format
 */
export function secondToTime(time) {
  let min = Math.floor(time / 60);
  let sec = time - min * 60;
  if (min < 10) min = `0${min}`;
  if (sec < 10) sec = `0${sec}`;
  return `${min}:${sec}`;
}

/**
 * Convert a timestamp in second to a more readable DD:MM:YYYY date format
 *
 * @param {Number} timestamp Timestamp in seconds
 * @return Date in DD:MM:YYYY format
 */
export function timestampToDate(timestamp) {
  let date = new Date(timestamp * 1000);
  return `${date.getDate()}/${date.getMonth()}/${date.getFullYear()}`;
}
