import React, { Component } from 'react';
import { Switch, Route, Redirect } from 'react-router-dom';
import { UserProvider } from './UserContext';
import { AuthenticatedRoute, UnauthenticatedRoute } from './AuthGuards';
import SignIn from './pages/SignIn';
import SignUp from './pages/SignUp';
import Home from './pages/Home';
import Albums from './pages/Albums';
import Album from './pages/Album';
import Artists from './pages/Artists';
import Artist from './pages/Artist';
import Favorites from './pages/Favorites';
import Playlist from './pages/Playlist';
import Preferences from './pages/Preferences';
import Admin from './pages/Admin';
import PageNotFound from './pages/PageNotFound';

export default class App extends Component {
  render() {
    return (
      <UserProvider>
        <Switch>
          <UnauthenticatedRoute strict path='/signin' component={SignIn} />
          <UnauthenticatedRoute strict path='/signup' component={SignUp} />
          {/* <Route strict path='/home' component={Home} /> */}
          <Route strict exact path='/albums' component={Albums} />
          <Route strict exact path='/albums/:id' component={Album} />
          <Route strict exact path='/artists' component={Artists} />
          <Route strict exact path='/artists/:id' component={Artist} />
          <AuthenticatedRoute strict path='/favorites' component={Favorites} />
          <AuthenticatedRoute strict path='/playlists/:id' component={Playlist} />
          <AuthenticatedRoute strict path='/preferences' component={Preferences} />
          <AuthenticatedRoute strict path='/admin' component={Admin} />
          <Route exact path='/'>
            <Redirect to='/albums' />
          </Route>
          <Route path='*' component={PageNotFound} />
        </Switch>
      </UserProvider>
    );
  }
}
