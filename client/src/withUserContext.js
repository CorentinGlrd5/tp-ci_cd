import { UserConsumer } from './UserContext';

export const withUserContext = (Component) => {
  return (props) => {
    return (
      <UserConsumer>
        {(user) => {
          return <Component {...props} {...user} />;
        }}
      </UserConsumer>
    );
  };
};
