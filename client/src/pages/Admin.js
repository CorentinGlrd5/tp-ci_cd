import { Component } from 'react';
import { Row, Form, Input, InputNumber, Button, DatePicker, Select, Col } from 'antd';
import DizifyLayout from '../components/DizifyLayout';

const form = {
  labelCol: { span: 8 },
  wrapperCol: { span: 16 },
};

export default class Admin extends Component {
  addArtist = (values) => {
    console.log(values);
  };

  addAlbulm = (values) => {
    console.log(values);
  };

  addTitle = (values) => {
    console.log(values);
  };

  render() {
    return (
      <DizifyLayout>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={8}>
            <Form
              {...form}
              name='nest-messages'
              onFinish={this.addArtist}
              className='form-container'>
              <Form.Item name={['user', 'name']} label='Name' rules={[{ required: true }]}>
                <Input />
              </Form.Item>
              <Form.Item name={['image', 'image']} label='Image (URI)'>
                <Input />
              </Form.Item>
              <Form.Item wrapperCol={{ ...form.wrapperCol, offset: 8 }}>
                <Button type='primary' htmlType='submit'>
                  Add an Artist
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col span={8}>
            <Form
              {...form}
              name='nest-messages'
              onFinish={this.addAlbulm}
              className='form-container'>
              <Form.Item name={['user', 'name']} label='Name' rules={[{ required: true }]}>
                <Input />
              </Form.Item>
              <Form.Item name={['date', 'date']} label='Date'>
                <DatePicker />
              </Form.Item>
              <Form.Item name={['image', 'image']} label='Image (URI)'>
                <Input />
              </Form.Item>
              <Form.Item name={['artist', 'artist']} label='Artist'>
                <Select>
                  <Select.Option value='david'>David Guetta</Select.Option>
                  <Select.Option value='shaka'>Shaka Ponk</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item wrapperCol={{ ...form.wrapperCol, offset: 8 }}>
                <Button type='primary' htmlType='submit' onClick={this.addAlbulm}>
                  Add an album
                </Button>
              </Form.Item>
            </Form>
          </Col>
          <Col span={8}>
            <Form
              {...form}
              name='nest-messages'
              onFinish={this.addTitle}
              className='form-container'>
              <Form.Item name={['name', 'name']} label='Name' rules={[{ required: true }]}>
                <Input />
              </Form.Item>
              <Form.Item name={['duration', 'time']} label='Duration (s)'>
                <InputNumber />
              </Form.Item>
              <Form.Item name={['artist', 'artist']} label='Artist'>
                <Select>
                  <Select.Option value='david'>David Guetta</Select.Option>
                  <Select.Option value='shaka'>Shaka Ponk</Select.Option>
                </Select>
              </Form.Item>
              <Form.Item wrapperCol={{ ...form.wrapperCol, offset: 8 }}>
                <Button type='primary' htmlType='submit'>
                  Add a title
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}
