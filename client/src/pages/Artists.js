import React, { Component } from 'react';
import { Row, Col } from 'antd';
import axios from 'axios';
import DizifyLayout from '../components/DizifyLayout';
import ArtistList from '../components/ArtistList';
import { withUserContext } from '../withUserContext';

class Artists extends Component {
  state = {
    artists: [],
  };

  async componentDidMount() {
    await this.getArtists();
  }

  async getArtists() {
    // const { isLoggedIn, user, token } = this.props;
    let artists = await axios.get(`${process.env.REACT_APP_API_URL}/artists`);
    this.setState({
      artists: artists.data,
    });
    // if (isLoggedIn) {
    //   let favorites = await axios.get(`${process.env.REACT_APP_API_URL}/user/${user.id}/artist`, {
    //     headers: { Authorization: `Bearer ${token}` },
    //   });
    //   let artistsWithFavorite = this.state.artists.map((item) => {
    //     const isFavorite = favorites.data.find((fav) => fav.id === item.id);
    //     return isFavorite ? { ...item, isFavorite: true } : { ...item, isFavorite: false };
    //   });
    //   this.setState({
    //     artists: artistsWithFavorite,
    //   });
    // }
  }

  render() {
    return (
      <DizifyLayout selected='artists'>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <ArtistList data={this.state.artists} />
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}

export default withUserContext(Artists);
