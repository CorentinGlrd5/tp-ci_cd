import React, { Component } from 'react';
import { Layout, Form, Input, Button, Row, Col } from 'antd';
import { UserOutlined, LockOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import { withUserContext } from '../withUserContext';
import Header from '../components/Header';
import axios from 'axios';

export class SignIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: this.props.error,
    };
  }

  onFinish = async (values) => {
    const { login, setUser, setPlaylists, setAlbums, setArtists, setTitles } = this.props;
    try {
      let signin = await axios.post(`${process.env.REACT_APP_API_URL}/signin`, values);
      login(signin.data.token);
      let user = await axios.get(`${process.env.REACT_APP_API_URL}/users/${signin.data.id}`, {
        headers: { Authorization: `Bearer ${signin.data.token}` },
      });
      const { id, username, avatar, role, playlists, albums, artists, titles } = user.data;
      setUser({ id, username, avatar, role, albums, artists, titles });
      setPlaylists(playlists);
      setAlbums(albums);
      setArtists(artists);
      setTitles(titles);
      this.props.history.push('/');
    } catch (error) {
      this.setState({ error: 'Wrong credentials' });
    }
  };

  render() {
    return (
      <Layout style={{ height: '100%' }}>
        <Header selected='signin' />
        <Row type='flex' justify='center' align='middle' style={{ height: '100%' }}>
          <Col span={12}>
            <Form name='signin' size='large' onFinish={this.onFinish}>
              <Form.Item
                name='username'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Username!',
                  },
                ]}>
                <Input
                  id='username-input'
                  prefix={<UserOutlined className='site-form-item-icon' />}
                  placeholder='Username'
                />
              </Form.Item>
              <Form.Item
                name='password'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Password!',
                  },
                ]}>
                <Input
                  prefix={<LockOutlined className='site-form-item-icon' />}
                  id='password-input'
                  type='password'
                  placeholder='Password'
                />
              </Form.Item>
              <Form.Item>
                <Button type='primary' htmlType='submit' id='signin-button'>
                  Sign in
                </Button>
              </Form.Item>
              <h1 id='error-message'>{this.state.error}</h1>
              <Form.Item>
                New to Dizify ? <Link to='/signup'>Create an account.</Link>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Layout>
    );
  }
}

export default withUserContext(SignIn);
