import React, { Component } from 'react';
import { Row, Col, Divider } from 'antd';
import DizifyLayout from '../components/DizifyLayout';
import ArtistList from '../components/ArtistList';
import AlbumList from '../components/AlbumList';

const artists = [{ name: 'Artist' }, { name: 'Artist' }, { name: 'Artist' }, { name: 'Artist' }];
const albums = [
  { title: 'Title', artist: 'Artist' },
  { title: 'Title', artist: 'Artist' },
  { title: 'Title', artist: 'Artist' },
  { title: 'Title', artist: 'Artist' },
];

export default class Home extends Component {
  render() {
    return (
      <DizifyLayout selected='home'>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <Divider orientation='left'>New Release</Divider>
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <AlbumList data={albums} />
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <Divider orientation='left'>Trending Artists</Divider>
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <ArtistList data={artists} />
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}
