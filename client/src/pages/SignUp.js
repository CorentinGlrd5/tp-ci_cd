import { LockOutlined, UserOutlined } from '@ant-design/icons';
import { Button, Col, Form, Input, Layout, Row } from 'antd';
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Header from '../components/Header';
import axios from 'axios';

export default class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: this.props.error,
    }
  }
  onFinish = async (values) => {
    console.log(values);
    let signUp = await axios.post(`${process.env.REACT_APP_API_URL}/signup`, values).catch((error) => {
      this.setState({ error: 'Username already exist !' });
      return 'error';
    });

    if (signUp === 'error') {
      return;
    }

    this.props.history.push('/signin');
  };

  render() {
    return (
      <Layout style={{ height: '100%' }}>
        <Header selected='signup' />
        <Row type='flex' justify='center' align='middle' style={{ height: '100%' }}>
          <Col span={12}>
            <Form name='signup' size='large' onFinish={this.onFinish}>
              <Form.Item
                name='username'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Username!',
                  },
                ]}>
                <Input id='username-input' prefix={<UserOutlined />} placeholder='Username' />
              </Form.Item>
              <Form.Item
                name='password'
                rules={[
                  {
                    required: true,
                    message: 'Please input your Password!',
                  },
                ]}>
                <Input id='password-input' prefix={<LockOutlined />} type='password' placeholder='Password' />
              </Form.Item>

              <Form.Item>
                <Button id='signup-button' type='primary' htmlType='submit'>
                  Create an account
                </Button>
              </Form.Item>
              <h1 id='error-message'>{this.state.error}</h1>
              <Form.Item>
                Already have an account ? <Link to='/signin'>Sign in</Link>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </Layout>
    );
  }
}
