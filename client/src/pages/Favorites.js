import React, { Component } from 'react';
import { Tabs, Row, Col } from 'antd';
import axios from 'axios';
import { withUserContext } from '../withUserContext';
import DizifyLayout from '../components/DizifyLayout';
import TitleList from '../components/TitleList';
import ArtistList from '../components/ArtistList';
import AlbumList from '../components/AlbumList';

class Favorites extends Component {
  state = {
    selected: 'artists',
    artists: [],
    albums: [],
    titles: [],
  };

  async componentDidMount() {
    await this.getUserData();
  }

  onTabClick = (key) => {
    this.setState({ selected: key });
  };

  getUserData = async () => {
    let res = await axios.get(`${process.env.REACT_APP_API_URL}/users/${this.props.user.id}`, {
      headers: { Authorization: `Bearer ${this.props.token}` },
    });
    this.setState({ artists: res.data.artists, albums: res.data.albums, titles: res.data.titles });
  };

  render() {
    const { artists, albums, titles } = this.state;

    return (
      <DizifyLayout selected='favorites'>
        <Tabs defaultActiveKey={this.state.selected} centered onTabClick={this.onTabClick}>
          <Tabs.TabPane tab='Artists' key='artists'>
            <Row gutter={[30, 30]} justify='start' align='start'>
              <Col span={24}>
                <ArtistList data={artists} />
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab='Albums' key='albums'>
            <Row gutter={[30, 30]} justify='start' align='start'>
              <Col span={24}>
                <AlbumList data={albums} />
              </Col>
            </Row>
          </Tabs.TabPane>
          <Tabs.TabPane tab='Titles' key='titles' forceRender>
            <Row gutter={[30, 30]} justify='start' align='start'>
              <Col span={24}>
                <TitleList data={titles} />
              </Col>
            </Row>
          </Tabs.TabPane>
        </Tabs>
      </DizifyLayout>
    );
  }
}

export default withUserContext(Favorites);
