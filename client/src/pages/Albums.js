import React, { Component } from 'react';
import { Row, Col } from 'antd';
import axios from 'axios';
import DizifyLayout from '../components/DizifyLayout';
import AlbumList from '../components/AlbumList';
import { withUserContext } from '../withUserContext';

class Albums extends Component {
  state = {
    albums: [],
  };

  async componentDidMount() {
    await this.getAlbums();
  }

  async getAlbums() {
    let albums = await axios.get(`${process.env.REACT_APP_API_URL}/albums`);
    this.setState({
      albums: albums.data,
    });
  }

  render() {
    return (
      <DizifyLayout selected='albums'>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <AlbumList data={this.state.albums} />
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}

export default withUserContext(Albums);
