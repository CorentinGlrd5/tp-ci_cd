import { Col, Divider, Row } from 'antd';
import React, { Component } from 'react';
import DizifyLayout from '../components/DizifyLayout';
import FavoriteButton from '../components/FavoriteButton';
import AlbumDetails from '../components/AlbumDetails';
import { withUserContext } from '../withUserContext';
import axios from 'axios';
import Artists from './Artists';

class Artist extends Component {
  state = {
    artist: { albums: [] },
  };

  async componentDidMount() {
    await this.getArtist();
  }

  async getArtist() {
    const { isLoggedIn, user, token, artists, albums } = this.props;
    let artist = await axios.get(`${process.env.REACT_APP_API_URL}${this.props.location.pathname}`);
    this.setState({
      artist: artist.data,
    });
    if (isLoggedIn) {
      let isArtistFavorite = artists.filter((artist) => artist.id === this.state.artist.id)[0];
      isArtistFavorite !== undefined
        ? this.setState({ artist: { ...this.state.artist, isFavorite: true } })
        : this.setState({ artist: { ...this.state.artist, isFavorite: false } });
      let albumsWithFavorites = this.state.artist.albums.map((album) => {
        let isFavorite = albums.filter((fav) => fav.id === album.id);
        return isFavorite.length !== 0
          ? { ...album, isFavorite: true }
          : { ...album, isFavorite: false };
      });
      this.setState({ artist: { ...this.state.artist, albums: albumsWithFavorites } });
    }
  }

  onToggleFavorite = async (favorite) => {
    const { user, token, artists, setArtists } = this.props;
    const { isFavorite, albums, ...artist } = this.state.artist;
    if (!favorite) {
      await axios.post(`${process.env.REACT_APP_API_URL}/users/${user.id}/artists`, artist, {
        headers: { Authorization: `Bearer ${token}` },
      });
      setArtists([...artists, artist]);
    } else {
      await axios.delete(`${process.env.REACT_APP_API_URL}/users/${user.id}/artists`, {
        data: artist,
        headers: { Authorization: `Bearer ${token}` },
      });
      setArtists(artists.filter((fav) => fav.id !== artist.id));
    }
  };

  render() {
    const { isLoggedIn, user, token } = this.props;
    const { artist } = this.state;

    return (
      <DizifyLayout>
        <Row gutter={[30, 30]} justify='start' align='middle'>
          <Col>
            <img
              style={{ width: '200px', height: '200px', borderRadius: '50%' }}
              src={artist.image}
              alt={artist.name}
            />
          </Col>
          <Col>
            <Row>
              <p style={{ fontSize: '2em', fontWeight: '500', margin: 0 }}>{artist.name}</p>
            </Row>
            {isLoggedIn && (
              <FavoriteButton
                type='ghost'
                isFavorite={artist.isFavorite}
                onToggleFavorite={this.onToggleFavorite}
                text='Add'
              />
            )}
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <Divider orientation='left'>Albums</Divider>
          </Col>
        </Row>
        {artist.albums.map((album) => {
          return (
            <AlbumDetails
              key={album.id}
              album={album}
              isLoggedIn={isLoggedIn}
              user={user}
              token={token}
            />
          );
        })}
      </DizifyLayout>
    );
  }
}

export default Artist = withUserContext(Artist);
