import React, { Component } from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';
import Error from '../img/error.svg';

export default class PageNotFound extends Component {
  render() {
    return (
      <div
        style={{
          width: '100%',
          height: '100%',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}>
        <img src={Error} alt='page not found' />
        <Button style={{ marginTop: '50px' }} type='primary'>
          <Link to='/'>Back to home</Link>
        </Button>
      </div>
    );
  }
}
