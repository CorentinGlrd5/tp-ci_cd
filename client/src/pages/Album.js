import React, { Component } from 'react';
import axios from 'axios';
import { Row, Col, Avatar } from 'antd';
import { withUserContext } from '../withUserContext';
import TitleList from '../components/TitleList';
import FavoriteButton from '../components/FavoriteButton';
import DizifyLayout from '../components/DizifyLayout';
import { timestampToDate, secondToTime } from '../utils/time';
class Album extends Component {
  state = {
    album: {
      artist: {},
      titles: [],
    },
  };

  async componentDidMount() {
    await this.getAlbum();
  }

  async getAlbum() {
    const { isLoggedIn, user, token, albums } = this.props;
    let album = await axios.get(`${process.env.REACT_APP_API_URL}${this.props.location.pathname}`);
    this.setState({
      album: album.data,
    });
    if (isLoggedIn) {
      let isAlbumFavorite = albums.filter((album) => album.id === this.state.album.id)[0];
      isAlbumFavorite !== undefined
        ? this.setState({ album: { ...this.state.album, isFavorite: true } })
        : this.setState({ album: { ...this.state.album, isFavorite: false } });
    }
  }

  getTotalTime(array) {
    let total = array.reduce((total, track) => {
      return total + track.time;
    }, 0);
    return secondToTime(total);
  }

  onToggleFavorite = async (favorite) => {
    const { user, token, albums, setAlbums } = this.props;
    const { isFavorite, artist, titles, ...album } = this.state.album;
    if (!favorite) {
      await axios.post(`${process.env.REACT_APP_API_URL}/users/${user.id}/albums`, album, {
        headers: { Authorization: `Bearer ${token}` },
      });
      setAlbums([...albums, album]);
    } else {
      await axios.delete(`${process.env.REACT_APP_API_URL}/users/${user.id}/albums`, {
        data: album,
        headers: { Authorization: `Bearer ${token}` },
      });
      setAlbums(albums.filter((fav) => fav.id !== album.id));
    }
  };

  render() {
    const { isLoggedIn } = this.props;
    const { album } = this.state;

    return (
      <DizifyLayout>
        <Row gutter={[30, 30]} justify='start' align='top'>
          <Col>
            <img
              style={{ width: '200px', height: '200px' }}
              src={album.image}
              alt={album.name || 'N/A'}
            />
          </Col>
          <Col>
            <Row>
              <Col>
                <p style={{ fontSize: '2em', fontWeight: '500', marginBottom: 20 }}>
                  {album.name || 'N/A'}
                </p>
              </Col>
            </Row>
            <Row>
              <Avatar shape='circle' src={album.artist.image} />
              <p style={{ fontSize: '1.5em', margin: '0px 10px', fontWeight: '300' }}>
                {album.artist.name}
              </p>
            </Row>
            <Row>
              <Col style={{ fontSize: '1em', fontWeight: '200', marginTop: 85 }}>
                <span>{album.titles.length}</span>
                <span> | </span>
                <span>{this.getTotalTime(album.titles)}</span>
                <span> | </span>
                <span>{timestampToDate(album.date)}</span>
              </Col>
            </Row>
          </Col>
        </Row>
        {isLoggedIn && (
          <Row gutter={[30, 30]} justify='start' align='start'>
            <Col>
              <FavoriteButton
                type='ghost'
                isFavorite={album.isFavorite}
                onToggleFavorite={this.onToggleFavorite}
                text='Add'
              />
            </Col>
          </Row>
        )}
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <TitleList data={album.titles} />
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}

export default Album = withUserContext(Album);
