import React, { Component } from 'react';
import { Form, Input, Button, Row, Col } from 'antd';
import DizifyLayout from '../components/DizifyLayout';
import { withUserContext } from '../withUserContext';

const formLayout = {
  labelCol: { span: 24 },
  wrapperCol: { span: 24 },
};

class Preferences extends Component {
  onFinish = (values) => {
    console.log(values);
  };

  onLogout = () => {
    this.props.logout();
    this.props.history.push('/signin');
  };

  render() {
    return (
      <DizifyLayout selected='profile'>
        <Row gutter={[30, 30]} justify='end' align='bottom'>
          <Col>
            <Button danger onClick={this.onLogout}>
              Logout
            </Button>
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='top'>
          <Col span={24}>
            <Form {...formLayout} labelAlign='left' name='register' onFinish={this.onFinish}>
              <Form.Item
                name='avatar'
                label='Avatar'
                rules={[
                  {
                    type: 'url',
                    message: 'The input is not valid URL !',
                  },
                ]}>
                <Input type='url' />
              </Form.Item>
              <Form.Item name='username' label='Username'>
                <Input />
              </Form.Item>

              <Form.Item
                name='password'
                label='Password'
                rules={[
                  {
                    required: true,
                    message: 'Please input your password !',
                  },
                ]}
                hasFeedback>
                <Input.Password />
              </Form.Item>

              <Form.Item
                name='confirm'
                label='Confirm Password'
                dependencies={['password']}
                hasFeedback
                rules={[
                  {
                    required: true,
                    message: 'Please confirm your password !',
                  },
                  ({ getFieldValue }) => ({
                    validator(rule, value) {
                      if (!value || getFieldValue('password') === value) {
                        return Promise.resolve();
                      }
                      return Promise.reject('The two passwords that you entered do not match!');
                    },
                  }),
                ]}>
                <Input.Password />
              </Form.Item>

              <Form.Item>
                <Button type='primary' htmlType='submit'>
                  Save
                </Button>
              </Form.Item>
            </Form>
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}

export default withUserContext(Preferences);
