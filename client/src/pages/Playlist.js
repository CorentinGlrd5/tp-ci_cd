import { Col, Row } from 'antd';
import axios from 'axios';
import React, { Component } from 'react';
import DizifyLayout from '../components/DizifyLayout';
import TitleList from '../components/TitleList';
import { withUserContext } from '../withUserContext';

class Playlist extends Component {
  state = {
    playlist: { titles: [] },
  };

  async componentDidMount() {
    await this.getPlaylist();
  }

  async getPlaylist() {
    let res = await axios.get(`${process.env.REACT_APP_API_URL}${this.props.location.pathname}`, {
      headers: { Authorization: `Bearer ${this.props.token}` },
    });
    this.setState({
      playlist: res.data,
    });
  }

  render() {
    const { isLoggedIn, location } = this.props;
    const { playlist } = this.state;

    return (
      <DizifyLayout selected={`playlist-${location.pathname.split('/')[2]}`}>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <TitleList data={playlist.titles} isLoggedIn={isLoggedIn} isPlaylist />
          </Col>
        </Row>
      </DizifyLayout>
    );
  }
}

export default withUserContext(Playlist);
