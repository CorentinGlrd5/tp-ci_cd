import React, { Component } from 'react';
import { Col, Row } from 'antd';
import FavoriteButton from '../components/FavoriteButton';
import { withUserContext } from '../withUserContext';
import TitleList from '../components/TitleList';
import axios from 'axios';

class AlbumDetails extends Component {
  onToggleFavoriteAlbum = async (favorite) => {
    const { user, token, setAlbums, albums } = this.props;
    const { isFavorite, titles, ...album } = this.props.album;

    if (!favorite) {
      await axios.post(`${process.env.REACT_APP_API_URL}/users/${user.id}/albums`, album, {
        headers: { Authorization: `Bearer ${token}` },
      });
      console.log(albums);
      setAlbums([...albums, album]);
    } else {
      await axios.delete(`${process.env.REACT_APP_API_URL}/users/${user.id}/albums`, {
        data: album,
        headers: { Authorization: `Bearer ${token}` },
      });
      setAlbums(albums.filter((fav) => fav.id !== album.id));
    }
  };

  render() {
    const { album, isLoggedIn } = this.props;
    return (
      <>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col>
            <img src={album.image} alt='ok' />
          </Col>
          <Col>
            <p style={{ fontSize: '2em', fontWeight: '400' }}>{album.name}</p>
            {isLoggedIn && (
              <FavoriteButton
                type='ghost'
                isFavorite={album.isFavorite}
                onToggleFavorite={this.onToggleFavoriteAlbum}
                text='Add'
              />
            )}
          </Col>
        </Row>
        <Row gutter={[30, 30]} justify='start' align='start'>
          <Col span={24}>
            <TitleList data={album.titles} />
          </Col>
        </Row>
      </>
    );
  }
}

export default withUserContext(AlbumDetails);
