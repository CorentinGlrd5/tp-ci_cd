import React, { Component } from 'react';
import { Table } from 'antd';
import AddToPlaylistModal from './AddToPlaylistModal';
import TitleActions from './TitleActions';
import { alphanum } from '../utils/alphanum';
import { secondToTime } from '../utils/time';
import axios from 'axios';
import { withUserContext } from '../withUserContext';

class TitleList extends Component {
  state = {
    visible: false,
    titles: [],
    track: {},
  };

  componentDidMount() {
    this.getFavorites();
  }

  componentDidUpdate(prevProps) {
    if (prevProps.data !== this.props.data) this.getFavorites();
  }

  getFavorites() {
    const { isLoggedIn, user, token, data, titles } = this.props;
    if (isLoggedIn) {
      let titlesWithFavorites = data.map((title) => {
        let isFavorite = titles.filter((fav) => fav.id === title.id);
        return isFavorite.length !== 0
          ? { ...title, isFavorite: true }
          : { ...title, isFavorite: false };
      });
      this.setState({ titles: titlesWithFavorites });
    } else {
      this.setState({ titles: this.props.data });
    }
  }

  cols = [
    {
      title: 'Title',
      dataIndex: 'name',
      key: 'name',
      width: '70%',
      sorter: (a, b) => alphanum(a.name, b.name),
      sortDirections: ['descend', 'ascend'],
    },
    {
      title: 'Time',
      dataIndex: 'time',
      key: 'time',
      sorter: (a, b) => a.time - b.time,
      sortDirections: ['descend', 'ascend'],
      render: (time) => secondToTime(time),
    },
    {
      title: '',
      key: 'action',
      align: 'right',
      render: (track) => (
        <TitleActions
          track={track}
          isPlaylist={this.props.isPlaylist ? true : false}
          onToggleModal={this.onToggleModal}
        />
      ),
    },
  ];

  columns = this.cols.filter((col) => {
    if (!this.props.isLoggedIn) {
      return col.key !== 'action';
    } else {
      return true;
    }
  });

  onToggleModal = (visible, track) => {
    this.setState({
      visible: visible,
      track: track,
    });
  };

  onAddToPlaylist = (playlists, track) => {
    const { isFavorite, users_titles, ...title } = track;
    for (let id in playlists) {
      axios
        .post(`${process.env.REACT_APP_API_URL}/playlists/${playlists[id]}/titles`, title, {
          headers: { Authorization: `Bearer ${this.props.token}` },
        })
        .then((res) => {
          if (res.status === 200) {
            console.log(res);
          } else {
            console.log(res);
            throw new Error('An error occured');
          }
        });
    }
  };

  render() {
    return (
      <>
        <AddToPlaylistModal
          visible={this.state.visible}
          track={this.state.track}
          onToggleModal={this.onToggleModal}
          onAddToPlaylist={this.onAddToPlaylist}
        />
        <Table
          rowKey='name'
          columns={this.columns}
          dataSource={this.state.titles}
          pagination={false}
        />
      </>
    );
  }
}

export default withUserContext(TitleList);
