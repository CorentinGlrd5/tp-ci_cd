import React, { Component } from 'react';
import { Layout, Menu } from 'antd';
import {
  HeartFilled,
  PlusCircleOutlined,
  ProfileOutlined,
  UserOutlined,
  UnorderedListOutlined,
  HomeOutlined,
  SettingOutlined,
} from '@ant-design/icons';
import { Link } from 'react-router-dom';
import CreatePlaylistModal from './CreatePlaylistModal';
import { withUserContext } from '../withUserContext';
import axios from 'axios';
export class Sider extends Component {
  state = {
    collapsed: false,
    visible: false,
  };

  onCollapse = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  };

  onToggleModal = (visible) => {
    this.setState({
      visible: visible,
    });
  };

  onCreatePlaylist = async (title) => {
    const { setPlaylists, playlists } = this.props;
    let res = await axios.post(
      `${process.env.REACT_APP_API_URL}/users/${this.props.user.id}/playlists`,
      {
        name: title,
      },
      { headers: { Authorization: `Bearer ${this.props.token}` } }
    );
    setPlaylists([...playlists, res.data]);
  };

  render() {
    const { isLoggedIn, playlists } = this.props;
    return (
      <Layout.Sider
        theme='light'
        onCollapse={this.onCollapse}
        collapsible
        collapsed={this.state.collapsed}>
        <CreatePlaylistModal
          visible={this.state.visible}
          onToggleModal={this.onToggleModal}
          onCreatePlaylist={this.onCreatePlaylist}
        />
        <Menu theme='light' mode='inline' defaultSelectedKeys={[this.props.selected || '']}>
          {/* <Menu.Item key='home' icon={<HomeOutlined />}>
            <Link to='/home'>Home</Link>
          </Menu.Item> */}
          <Menu.Item key='artists' icon={<UserOutlined />}>
            <Link to='/artists'>Artistes</Link>
          </Menu.Item>
          <Menu.Item key='albums' icon={<ProfileOutlined />}>
            <Link to='/albums'>Albums</Link>
          </Menu.Item>
          {isLoggedIn && (
            <>
              <Menu.Item key='favorites' icon={<HeartFilled />}>
                <Link to='/favorites'>Favorites</Link>
              </Menu.Item>
              <Menu.SubMenu key='playlists' icon={<UnorderedListOutlined />} title='Playlists'>
                {playlists.map((playlist) => (
                  <Menu.Item key={`playlist-${playlist.id}`}>
                    <Link to={`/playlists/${playlist.id}`}>{playlist.name}</Link>
                  </Menu.Item>
                ))}
              </Menu.SubMenu>
              <Menu.Item
                key='add'
                icon={<PlusCircleOutlined />}
                onClick={() => this.onToggleModal(true)}>
                New Playlist
              </Menu.Item>
              <Menu.Item key='preferences' icon={<SettingOutlined />}>
                <Link to='/preferences'>Preferences</Link>
              </Menu.Item>
            </>
          )}
        </Menu>
      </Layout.Sider>
    );
  }
}

export default withUserContext(Sider);
