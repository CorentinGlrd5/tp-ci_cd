import React, { Component } from 'react';
import { Button } from 'antd';
import { HeartFilled, HeartOutlined } from '@ant-design/icons';

export default class FavoriteButton extends Component {
  state = {
    isFavorite: this.props.isFavorite,
  };

  componentDidUpdate(prevProps) {
    if (prevProps.isFavorite !== this.props.isFavorite)
      this.setState({ isFavorite: this.props.isFavorite });
  }

  onToggleFavorite = () => {
    this.setState({
      isFavorite: !this.state.isFavorite,
    });
    this.props.onToggleFavorite(this.state.isFavorite);
  };

  render() {
    return (
      <Button
        type={this.props.type}
        block={this.props.block}
        ghost={this.props.ghost}
        shape={this.props.shape}
        onClick={() => this.onToggleFavorite()}
        icon={
          this.state.isFavorite ? (
            <HeartFilled style={{ color: '#1890ff' }} />
          ) : (
            <HeartOutlined style={{ color: '#1890ff' }} />
          )
        }>
        {this.props.text || ''}
      </Button>
    );
  }
}
