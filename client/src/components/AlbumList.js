import React, { Component } from 'react';
import { List } from 'antd';
import { Link } from 'react-router-dom';

export default class AlbumList extends Component {
  render() {
    return (
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 4,
          xxl: 5,
        }}
        dataSource={this.props.data}
        renderItem={(album) => (
          <List.Item>
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Link to={`/albums/${album.id}`}>
                <img
                  style={{ width: '200px', height: '200px' }}
                  src={album.image}
                  alt={album.name}
                />
              </Link>
              <p style={{ fontSize: '1.4em', margin: '0' }}>
                <Link to={`/albums/${album.id}`}>{album.name}</Link>
              </p>
              <p style={{ fontSize: '0.8em', margin: '0' }}>
                <Link to={`/artists/${album.artist.id}`}>{album.artist.name}</Link>
              </p>
            </div>
          </List.Item>
        )}
      />
    );
  }
}
