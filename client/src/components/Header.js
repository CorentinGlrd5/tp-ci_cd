import React, { Component } from 'react';
import { Menu, Avatar, Button } from 'antd';
import { UserOutlined } from '@ant-design/icons';
import { Link } from 'react-router-dom';
import DizifyLogo from '../img/icon.svg';
import { withUserContext } from '../withUserContext';

export class Header extends Component {
  render() {
    const { isLoggedIn } = this.props;
    return (
      <Menu theme='light' mode='horizontal'>
        <Menu.Item>
          <Link to='/home'>
            <img src={DizifyLogo} alt='dizify logo' style={{ width: '30px', height: '30px' }} />
          </Link>
        </Menu.Item>
        {isLoggedIn && (
          <Menu.Item style={{ float: 'right' }}>
            <Link to='/preferences'>
              <Avatar icon={<UserOutlined />} />
            </Link>
          </Menu.Item>
        )}
        {!isLoggedIn && (
          <>
            <Menu.Item style={{ float: 'right' }}>
              <Button type='primary'>
                <Link to='/signin'>Sign In</Link>
              </Button>
            </Menu.Item>
            <Menu.Item style={{ float: 'right' }}>
              <Button type='ghost'>
                <Link to='/signup'>Sign Up</Link>
              </Button>
            </Menu.Item>
          </>
        )}
      </Menu>
    );
  }
}

export default withUserContext(Header);
