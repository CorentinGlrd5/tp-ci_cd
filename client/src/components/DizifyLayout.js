import React, { Component } from 'react';
import { Layout } from 'antd';
import Header from './Header';
import Sider from './Sider';

export default class DizifyLayout extends Component {
  render() {
    return (
      <Layout style={{ height: '100%' }}>
        <Header />
        <Layout>
          <Sider selected={this.props.selected} />
          <Layout style={{ padding: '24px ' }}>
            <Layout.Content
              style={{
                backgroundColor: '#fff',
                padding: '50px',
                overflowY: 'auto',
              }}>
              {this.props.children}
            </Layout.Content>
          </Layout>
        </Layout>
      </Layout>
    );
  }
}
