import { Modal, Select } from 'antd';
import React, { Component } from 'react';
import { withUserContext } from '../withUserContext';

class AddToPlaylistModal extends Component {
  state = {
    playlists: [],
  };

  onChange = (selected) => {
    this.setState({
      playlists: selected,
    });
  };

  onFinish = () => {
    this.props.onAddToPlaylist(this.state.playlists, this.props.track);
    this.props.onToggleModal(false);
    this.setState({
      playlists: [],
    });
  };

  onCancel = () => {
    this.props.onToggleModal(false);
    this.setState({
      playlists: [],
    });
  };

  render() {
    const { playlists } = this.props;

    return (
      <Modal
        title='Add to Playlist'
        visible={this.props.visible}
        onOk={this.onFinish}
        onCancel={this.onCancel}>
        <Select
          mode='tags'
          style={{ width: '100%' }}
          onChange={this.onChange}
          tokenSeparators={[',']}>
          {playlists.map((playlist) => {
            return <Select.Option key={playlist.id}>{playlist.name}</Select.Option>;
          })}
        </Select>
      </Modal>
    );
  }
}

export default withUserContext(AddToPlaylistModal);
