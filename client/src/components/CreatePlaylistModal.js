import React, { Component } from 'react';
import { UnorderedListOutlined } from '@ant-design/icons';
import { Modal, Form, Input } from 'antd';

export default class CreatePlaylistModal extends Component {
  state = {
    title: '',
  };

  onFinish = () => {
    this.props.onCreatePlaylist(this.state.title);
    this.props.onToggleModal(false);
    this.setState({
      title: '',
    });
  };

  onCancel = () => {
    this.props.onToggleModal(false);
    this.setState({
      title: '',
    });
  };

  onValuesChange = ({ title }) => {
    this.setState({
      title,
    });
  };

  render() {
    return (
      <Modal
        title='Create Playlist'
        visible={this.props.visible}
        onOk={this.onFinish}
        onCancel={this.onCancel}>
        <Form name='playlist' size='large' onValuesChange={this.onValuesChange}>
          <Form.Item
            name='title'
            rules={[
              {
                required: true,
                message: 'Please input your playlist title!',
              },
            ]}>
            <Input prefix={<UnorderedListOutlined />} placeholder='Title' />
          </Form.Item>
        </Form>
      </Modal>
    );
  }
}
