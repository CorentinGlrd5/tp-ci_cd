import { PlusOutlined } from '@ant-design/icons';
import { Button } from 'antd';
import axios from 'axios';
import React, { Component } from 'react';
import { withUserContext } from '../withUserContext';
import FavoriteButton from './FavoriteButton';

class TitleActions extends Component {
  onToggleModal = (visible) => {
    this.props.onToggleModal(visible, this.props.track);
  };

  onToggleFavorite = async (favorite) => {
    const { user, titles, setTitles } = this.props;
    const { isFavorite, ...track } = this.props.track;
    if (!favorite) {
      await axios.post(`${process.env.REACT_APP_API_URL}/users/${user.id}/titles`, track, {
        headers: { Authorization: `Bearer ${this.props.token}` },
      });
      setTitles([...titles, track]);
    } else {
      await axios.delete(`${process.env.REACT_APP_API_URL}/users/${user.id}/titles`, {
        headers: { Authorization: `Bearer ${this.props.token}` },
      });
      setTitles(titles.filter((fav) => fav.id !== track.id));
    }
  };

  render() {
    return (
      <>
        <FavoriteButton
          type='text'
          isFavorite={this.props.track.isFavorite}
          onToggleFavorite={this.onToggleFavorite}
        />
        {!this.props.isPlaylist && (
          <Button
            type='text'
            icon={
              <PlusOutlined style={{ color: '#1890ff' }} onClick={() => this.onToggleModal(true)} />
            }
          />
        )}
      </>
    );
  }
}

export default TitleActions = withUserContext(TitleActions);
