import React, { Component } from 'react';
import { List } from 'antd';
import { Link } from 'react-router-dom';

export default class ArtistList extends Component {
  render() {
    return (
      <List
        grid={{
          gutter: 16,
          xs: 1,
          sm: 1,
          md: 2,
          lg: 3,
          xl: 4,
          xxl: 5,
        }}
        dataSource={this.props.data}
        renderItem={(artist) => (
          <List.Item>
            <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
              <Link to={`/artists/${artist.id}`}>
                <img
                  style={{ width: '200px', height: '200px', borderRadius: '50%' }}
                  src={artist.image}
                  alt={artist.name}
                />
              </Link>
              <p style={{ fontSize: '1.5em', margin: '11px 0 11px 0', textAlign: 'center' }}>
                <Link to={`/artists/${artist.id}`}>{artist.name}</Link>
              </p>
            </div>
          </List.Item>
        )}
      />
    );
  }
}
