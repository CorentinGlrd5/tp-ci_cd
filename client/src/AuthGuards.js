import { Route, Redirect } from 'react-router';
import { UserConsumer } from './UserContext';

export const AuthenticatedRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={(props) => (
        <UserConsumer>
          {(user) => (user.isLoggedIn ? <Component {...props} /> : <Redirect to={`/signin`} />)}
        </UserConsumer>
      )}
    />
  );
};

export const UnauthenticatedRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (
      <UserConsumer>
        {(user) => (!user.isLoggedIn ? <Component {...props} /> : <Redirect to='/' />)}
      </UserConsumer>
    )}
  />
);
