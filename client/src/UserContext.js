import React, { Component } from 'react';

const { Provider, Consumer } = React.createContext();

class UserProvider extends Component {
  state = {
    isLoggedIn: localStorage.getItem('isLoggedIn') || false,
    token: localStorage.getItem('token') || '',
    user: JSON.parse(localStorage.getItem('user')) || {},
    playlists: JSON.parse(localStorage.getItem('playlists')) || [],
    albums: JSON.parse(localStorage.getItem('albums')) || [],
    artists: JSON.parse(localStorage.getItem('artists')) || [],
    titles: JSON.parse(localStorage.getItem('titles')) || [],
  };

  setUser = (user) => {
    const { playlists, artists, albums, titles, password, ...rest } = user;
    this.setState({ user: rest });
    localStorage.setItem('user', JSON.stringify(this.state.user));
  };

  setPlaylists = (playlists) => {
    this.setState({ playlists });
    localStorage.setItem('playlists', JSON.stringify(this.state.playlists));
  };

  setAlbums = (albums) => {
    console.log(albums);
    this.setState({ albums });
    localStorage.setItem('albums', JSON.stringify(this.state.albums));
  };

  setArtists = (artists) => {
    this.setState({ artists });
    localStorage.setItem('artists', JSON.stringify(this.state.artists));
  };

  setTitles = (titles) => {
    this.setState({ titles });
    localStorage.setItem('titles', JSON.stringify(this.state.titles));
  };

  login = (token) => {
    this.setState({
      isLoggedIn: true,
      token: token,
    });
    localStorage.setItem('isLoggedIn', this.state.isLoggedIn);
    localStorage.setItem('token', this.state.token);
  };

  logout = () => {
    this.setState({
      isLoggedIn: false,
      token: '',
      user: {},
      playlists: [],
      albums: [],
      artists: [],
      titles: [],
    });
    localStorage.removeItem('isLoggedIn');
    localStorage.removeItem('token');
    localStorage.removeItem('user');
    localStorage.removeItem('playlists');
    localStorage.removeItem('albums');
    localStorage.removeItem('artists');
    localStorage.removeItem('titles');
  };

  render() {
    return (
      <Provider
        value={{
          isLoggedIn: this.state.isLoggedIn,
          token: this.state.token,
          user: this.state.user,
          playlists: this.state.playlists,
          albums: this.state.albums,
          artists: this.state.artists,
          titles: this.state.titles,
          login: this.login,
          logout: this.logout,
          setUser: this.setUser,
          setPlaylists: this.setPlaylists,
          setTitles: this.setTitles,
          setAlbums: this.setAlbums,
          setArtists: this.setArtists,
        }}>
        {this.props.children}
      </Provider>
    );
  }
}

export { UserProvider, Consumer as UserConsumer };
