context('Failed auth with bad credentials', () => {
    beforeEach(() => {
      cy.visit('/signin');
    });

    
    it('Failed auth', () => {
        cy.server({ method: 'POST' })
        cy.route('http://localhost:8000/signin').as('one')

        cy.get('input[id=username-input]')
            .type('user123456');
        
        cy.get('input[id=password-input]')
            .type('password123456');
        
        cy.get('button[id=signin-button]').click();

        cy.wait(['@one'], { timeout: 10000 });

        cy.get('h1[id=error-message]').contains('Wrong credentials')

    });
  });