context('Username already exist in database', () => {
    beforeEach(() => {
        cy.visit('/signup');
    });

    it('Failed auth', () => {
        cy.server({ method: 'POST' })
        cy.route('http://localhost:8000/signup').as('one')

        cy.get('input[id=username-input]')
            .type('user');

        cy.get('input[id=password-input]')
            .type('password');

        cy.get('button[id=signup-button]').click();

        cy.wait(['@one'], { timeout: 12000 });

        cy.get('h1[id=error-message]').contains('Username already exist !')

    });
});