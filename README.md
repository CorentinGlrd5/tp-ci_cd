# TP - CI_CD

## Membres du projet

-   Elouan LEFRECHOUX
-   Corentin GUILLARD

## Liste des fonctionnalités

### Front

-   Page d'accueil
-   Page de listing par artistes, par titres et par albums
-   Page de favoris

### Back

-   Mise en place d'une BDD avec les tables suivantes:

    -   Album
    -   Artist
    -   Title
    -   Favorites
    -   User

-   Création des différentes routes pour chaque table: GET, POST, UPDATE et DELETE

-   Ajouter des tests des requêtes

-   Ajout de fausses données en base

## Schema du pipeline d'intégration et de déploiement continue

- Les étapes :

    -   Build
    -   Test
    -   Deploy